//类是对象具体事物的一个抽象，对象时类的具体表现
class XiaoJiejei{
    name:string
    age:number
    constructor(name:string,age:number){
        this.name=name
        this.age=age
    }

    say(){
        console.log("小哥哥好")
    }
}

let jieie:XiaoJiejei=new XiaoJiejei('fanbinbin',12);

console.log(jieie)
jieie.say()
