"use strict";
//不传递任何参数
//let d:Date=new Date()
//console.log(d);
//2 传递一个整数
/* let d:Date=new Date(1000)//1000 开始日期是1970 年
let d2:Date=new Date(2000)//1000 开始日期是1970 年
console.log(d)
console.log(d2) */
//3 传递一个字符串
/* let d1:Date=new Date('2018/09/07 05:35:00')
let d2:Date=new Date('2018-09-07 05:35:00')
let d3:Date=new Date('2018-09-07T05:35:00')

console.log(d1)
console.log(d2)
console.log(d3) */
//4 传递年  月  日   小时
