"use strict";
//类是对象具体事物的一个抽象，对象时类的具体表现
var XiaoJiejei = /** @class */ (function () {
    function XiaoJiejei(name, age) {
        this.name = name;
        this.age = age;
    }
    XiaoJiejei.prototype.say = function () {
        console.log("小哥哥好");
    };
    return XiaoJiejei;
}());
var jieie = new XiaoJiejei('fanbinbin', 12);
console.log(jieie);
jieie.say();
