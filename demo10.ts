//正则表达式
//RegExp string

//1、构造函数声明法
/* let reg1:RegExp=new RegExp("jspang")
console.log(reg1)
let reg2:RegExp=new RegExp("jspang","gi")
console.log(reg2) */

//2、字面量申明法
/* let reg3:RegExp=/jspang/i
let reg4:RegExp=/jspang/gi */

//正则表达式常用的方法
//test(string) exec(string)

let reg1:RegExp=/jspang/i
let website:string='jspang.com'

//let result1:boolean=reg1.test(website);

console.log(reg1.exec(website))
