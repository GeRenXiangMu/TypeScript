"use strict";
//1、
/* function searchXiaoJieJie(age:number):string{

 return '找到了'+age+'岁的小姐姐'
} */
//2有可选参数的函数
/* function searchXiaoJieJie(age:number,stature?:string):string{
    let yy:string=''
    yy='找到了'+age+'岁的小姐姐'
    if(stature!=undefined){
        yy=yy+stature
    }
    return yy+'的小姐姐';
}

var age:number=28
var result:string=searchXiaoJieJie(age)
console.log(result) */
// 3、有默认参数的函数
/* function searchXiaoJieJie(age:number=18,stature:string='水好好'):string{
    let yy:string=''
    yy='找到了'+age+'岁的小姐姐'
    if(stature!=undefined){
        yy=yy+stature
    }
    return yy+'的小姐姐';
}

var age:number=28
var result:string=searchXiaoJieJie(22,'大长腿')
console.log(result) */
//4、有剩余参数的函数
function searchXiaoJieJie() {
    var xuqiu = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        xuqiu[_i] = arguments[_i];
    }
    var yy = '找到了';
    for (var i = 0; i < xuqiu.length; i++) {
        yy = yy + xuqiu[i];
        if (i < xuqiu.length) {
            yy = yy + '、';
        }
    }
    return yy;
}
var result = searchXiaoJieJie('22岁', '大长腿', '爪子脸', '沙沙不不');
console.log(result);
