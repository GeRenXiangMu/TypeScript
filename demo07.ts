/* let jspang={
    name:'技术胖',
    website:'jspang.com',
    age:18,
    saySometing:function(){
        console.log('为了前端技术')
    }
}
console.log(jspang.name)
jspang.saySometing() */

/* let arr1:number[]
let arr2:Array<String> */

// 字面量赋值法
//构造函数赋值法
/* let arr1:number[]=[]
let arrr2:number[]=[1,2,3,4,5]

let arr3:Array<string>=['jspang','技术胖','金三胖']
let arr4:Array<boolean>=[true,false,false] */

//构造函数赋值法
let arr1:number[]=new Array()
let arrr2:number[]=new Array(1,2,3,4,5)

let arr3:Array<string>=new Array('jspang','技术胖','金三胖')
let arr4:Array<boolean>=new Array(true,false,false)

/* let arr5:number[]=[1,2,true] */

//元祖 工作中不推荐使用
let x:[string,number]
x=['hello',10]