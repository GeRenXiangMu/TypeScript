/* interface Husband{
    sex:string
    interest:string
    maiBaoBao?:Boolean//可选参数?
}

let myhusband:Husband={sex:'男',interest:'篮球，做家务',maiBaoBao:true}

console.log(myhusband) */
//用接口的方式来规范函数
interface searchMan{
    (source:string,subString:string):boolean
}

let mySearch:searchMan

mySearch=function(source:string,subString:string):boolean{
    let flag=source.search(subString)
    return (flag!=-1)
}

console.log(mySearch('高、富、帅、德、胖','胖'))