"use strict";
var age = 18;
var stature = 178.5;
console.log(age);
console.log(stature);
console.log('------------------------');
var jspang = "技术胖 jspang.com";
console.log(jspang);
//boolean true false
var b = true;
var c = false;
//enum 类型 枚举  人：男人，女人，中性   四季：
console.log("--------------------");
var REN;
(function (REN) {
    REN["nan"] = "\u7537\u4EBA";
    REN["nv"] = "\u5973\u4EBA";
    REN["yao"] = "\u5996";
})(REN || (REN = {}));
console.log(REN.yao);
//any 类型 任意类型
var t = 10;
t = 'jspang';
t = true;
console.log(t);
//null 
